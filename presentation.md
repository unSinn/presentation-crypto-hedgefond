Noser Crypto Hedgefonds
===

# ![100%](img/colored-coins.png)

##### Wer nicht wagt, der nicht gewinnt

###### Fabian Mächler ( fabian.maechler@noser.com )

---

# Noser Crypto-Hedgefonds
- Jeder kann CHF 100.- einzahlen
- Der Fonds bleibt 1 Jahr lang geschlossen
- Nach einem Jahr unternehmen wir etwas schönes
- Regelmässige Performance Audits

---
# How does it work?

- Langweiliges CHF zu Handen Fabian (Bar oder via IBAN)
- Fabian kauft Cryptos und legt diese in einem Paper Wallet ab
- Ihr sagt mir in welche Coins euer Geld investiert wird (Portfolio)
- Ihr geht davon aus, dass das Geld verloren ist
- Und nein, ich ziehe keine Krawatte an

---

![100%](img/ccm.png)

---
# ![100%](img/bitcoin.png) Bitcoin

- Der Klassiker
- Wird auch nicht so schnell verschwinden
- Alles andere orientiert sich an Bitcoin

---

# ![100%](img/ethereum.png) Etherium
- Die eierlegende Wollmilchsau
- Basis für diverse andere Coins

---
# ![100%](img/dash.png) Dash & Zcash
- Die Zahlungsmittel

---

# Und viele weitere mehr
- ![100%](img/augur.png) Augur für Wetten in die Zukunft
- ![100%](img/siacoin.png) Siacoin private Cloud
- ![100%](img/storj.png)  Storj Storage

---

# Präsentation verfügbar unter Git
Hosted "somewhere" in the smoggy Cloud. Username checks out.
https://gitlab.com/unSinn/presentation-crypto-hedgefond.git

For everything else click here:
https://nosercloud.sharepoint.com/SitePages/Vorschlagswesen.aspx

## Also: Cheers to the power of Markdown
Presentation made with Marp https://github.com/yhatt/marp ![50%](img/marp.png)